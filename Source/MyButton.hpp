//
//  MyButton.hpp
//  JuceBasicWindow
//
//  Created by Kyle Edwards on 20/10/2016.
//
//

#ifndef MyButton_hpp
#define MyButton_hpp

#include <stdio.h>
#include "/Volumes/DataDrive/Users/k35-edwards/Desktop/Juce/Practical 5/JuceLibraryCode/JuceHeader.h"

class MyButton : public Component
{
public:
    void paint (Graphics& g) override;
    void mouseUp (const MouseEvent& event) override;
    void mouseDown (const MouseEvent& event) override;
    void mouseEnter (const MouseEvent& event) override;
    void mouseExit (const MouseEvent& event) override;
    
private:
    Colour colour;
    
};

#endif /* MyButton_hpp */
