//
//  MyButton.cpp
//  JuceBasicWindow
//
//  Created by Kyle Edwards on 20/10/2016.
//
//

#include "MyButton.hpp"

void MyButton::paint (Graphics& g)
{
    g.setColour(colour);
    g.fillAll();
}
void MyButton::mouseUp(const MouseEvent& event)
{
    colour = Colours::beige;
    repaint();
}
void MyButton::mouseDown(const MouseEvent& event)
{
    colour = Colours::maroon;
    repaint();
}
void MyButton::mouseEnter(const MouseEvent& event)
{
    colour = Colours::khaki;
    repaint();
}
void MyButton::mouseExit(const MouseEvent& event)
{
    colour = Colours::greenyellow;
    repaint();
}